#include "utf8.hpp"
#include "unit_config.h"
#include <gtest/gtest.h>
#include <ciso646>
#include <string>
#include <fstream>
#include <vector>

namespace {
	void TestReadingFile (const char* parSourcePath) {
		// Open the test file
		std::ifstream fs8(parSourcePath);
		ASSERT_TRUE(fs8.is_open());

		// Read it line by line
		unsigned int line_count = 0;
		char byte;
		while (!fs8.eof()) {
			std::string line;
			while ((byte = static_cast<char>(fs8.get())) != '\n' && !fs8.eof())
				line.push_back(byte);

			line_count++;
			// Play around with each line and convert it to utf16
			std::string::iterator line_start = line.begin();
			std::string::iterator line_end   = line.end();
			line_end = utf8::find_invalid(line_start, line_end);
			EXPECT_EQ(line_end, line.end()) << "Line " << line_count << ": Invalid utf-8 at byte " << int(line.end() - line_end);

			// Convert it to utf-16 and write to the file
			std::vector<unsigned short> utf16_line;
			utf8::utf8to16(line_start, line_end, std::back_inserter(utf16_line));

			// Back to utf-8 and compare it to the original line.
			std::string back_to_utf8;
			utf8::utf16to8(utf16_line.begin(), utf16_line.end(), std::back_inserter(back_to_utf8));
			EXPECT_EQ(back_to_utf8.compare(std::string(line_start, line_end)), 0) <<"Line " << line_count << ": Conversion to UTF-16 and back failed";

			// Now, convert it to utf-32, back to utf-8 and compare
			std::vector <unsigned> utf32_line;
			utf8::utf8to32(line_start, line_end, std::back_inserter(utf32_line));
			back_to_utf8.clear();
			utf8::utf32to8(utf32_line.begin(), utf32_line.end(), std::back_inserter(back_to_utf8));
			EXPECT_EQ(back_to_utf8.compare(std::string(line_start, line_end)), 0) << "Line " << line_count << ": Conversion to UTF-32 and back failed";

			// Now, iterate and back
			unsigned char_count = 0;
			std::string::iterator it = line_start;
			while (it != line_end) {
				unsigned int next_cp = utf8::peek_next(it, line_end);
				EXPECT_EQ(utf8::next(it, line_end), next_cp) << "Line " << line_count << ": Error: peek_next gave a different result than next";
				char_count++;
			}
			EXPECT_EQ(char_count, utf32_line.size()) << "Line " << line_count << ": Error in iterating with next - wrong number of characters";

			std::string::iterator adv_it = line_start;
			utf8::advance(adv_it, char_count, line_end);
			EXPECT_EQ(adv_it, line_end) << "Line " << line_count << ": Error in advance function";

			EXPECT_EQ(std::string::size_type(utf8::distance(line_start, line_end)), char_count) << "Line " << line_count << ": Error in distance function";

			while (it != line_start) {
				utf8::previous(it, line.rend().base());
				char_count--;
			}
			EXPECT_EQ(char_count, 0) << "Line " << line_count << ": Error in iterating with previous - wrong number of characters";

			// Try utf8::iterator
			utf8::iterator<std::string::iterator> u8it(line_start, line_start, line_end);
			EXPECT_FALSE(not utf32_line.empty() and *u8it != utf32_line.at(0)) << "Line " << line_count << ": Error in utf::iterator * operator";
			const size_t calculatedDist = std::distance(u8it, utf8::iterator<std::string::iterator>(line_end, line_start, line_end));
			EXPECT_EQ(calculatedDist, static_cast<int>(utf32_line.size())) <<"Line " << line_count << ": Error in using utf::iterator with std::distance - wrong number of characters";

			std::advance(u8it, utf32_line.size());
			EXPECT_EQ(u8it, utf8::iterator<std::string::iterator>(line_end, line_start, line_end)) << "Line " << line_count << ": Error in using utf::iterator with std::advance";
		}
	}
} //unnamed namespace

TEST(Utf8, Reader) {
	TestReadingFile(PATH_UTF8_VALID1_TXT);
	TestReadingFile(PATH_UTF8_VALID2_HTML);
	TestReadingFile(PATH_UTF8_VALID3_TXT);
}
