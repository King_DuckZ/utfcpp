#include "utf8.hpp"
#include "unit_config.h"
#include <gtest/gtest.h>
#include <string>
#include <iterator>
#include <ciso646>
#include <fstream>
#include <algorithm>

static const unsigned INVALID_LINES[] = {
	75, 76, 83, 84, 85, 93, 102, 103, 105, 106, 107, 108, 109, 110, 114, 115,
	116, 117, 124, 125, 130, 135, 140, 145, 153, 154, 155, 156, 157, 158, 159,
	160, 161, 162, 169, 175, 176, 177, 207, 208, 209, 210, 211, 220, 221, 222,
	223, 224, 232, 233, 234, 235, 236, 247, 248, 249, 250, 251, 252, 253, 257,
	258, 259, 260, 261, 262, 263, 264
};
static const unsigned* const INVALID_LINES_END = INVALID_LINES + sizeof(INVALID_LINES)/sizeof(unsigned);

TEST(Negative, InvalidUtf) {
    // Open the test file
    std::ifstream fs8(PATH_UTF8_INVALID_TXT);
	ASSERT_TRUE(fs8.is_open());

    // Read it line by line
    unsigned int line_count = 0;
    char byte;
    while (!fs8.eof()) {
        std::string line;
        while ((byte = static_cast<char>(fs8.get())) != '\n' && !fs8.eof())
            line.push_back(byte);

        line_count++;
        const bool expected_valid = (std::find(INVALID_LINES, INVALID_LINES_END, line_count) == INVALID_LINES_END);
        // Print out lines that contain unexpected invalid UTF-8
		const bool valid = utf8::is_valid(line.begin(), line.end());
		EXPECT_EQ(valid, expected_valid);
        if (not valid) {
            // try fixing it:
            std::string fixed_line;
            utf8::replace_invalid(line.begin(), line.end(), std::back_inserter(fixed_line));
			EXPECT_TRUE(utf8::is_valid(fixed_line.begin(), fixed_line.end()));
        }
    }
}
